#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import absolute_import

__author__ = "Lluís Vilanova"
__copyright__ = "Copyright 2017-2018, Lluís Vilanova"
__license__ = "GPL version 3 or later"


import argparse
import collections
import os
import sys


def run(input_dir):
    frameworks = dict(
            ruby=["gitlab"],
            django=["datscoding"],
            python=["hackerslides"],
            rubyonrails=["gitlab"],
            php=["datscoding"],
            nodejs=["letschat", "datshealth", "datspdf", "datsimage"],
            golang=["mattermost"],
    )
    def default():
        return [0, 0]
    total = collections.defaultdict(default)
    total_leak = collections.defaultdict(default)

    print("File\t#CVEs\t#CVEs w/leak")
    for path in os.listdir(input_dir):
        path = os.path.join(input_dir, path)
        name = os.path.split(path)[-1].split(".")[0]

        if not os.path.isfile(path):
            continue
        if not path.endswith(".txt"):
            continue

        with open(path, "r") as f:
            CVEs = 0
            CVEs_leak = 0

            lines = f.readlines()

            if name == "mattermost":
                # https://about.mattermost.com/security-updates/
                def ignore(header, fields):
                    return False
                def is_leak(header, fields):
                    leak_idx = header.index("cross-folder")
                    leak = fields[leak_idx]
                    return leak == "Y"
            else:
                # https://www.cvedetails.com/
                def ignore(header, fields):
                    cve_idx = header.index("CVE ID")
                    cve = fields[cve_idx].split("-")[1]
                    return cve not in ["2017", "2016", "2015", "2014", "2013"]
                def is_leak(header, fields):
                    leak_idx = header.index("Conf.")
                    leak = fields[leak_idx]
                    return leak != "None"

            header = lines[0].split("\t")
            for line in lines[1:]:
                if line.endswith("\n"):
                    line = line[:-1]
                fields = line.split("\t")

                if ignore(header, fields):
                    continue

                CVEs += 1
                if is_leak(header, fields):
                    CVEs_leak += 1

        print(path, "\t", CVEs, "\t", CVEs_leak)

        if name not in frameworks:
            total[name][0] += CVEs
            total[name][1] += CVEs
            total_leak[name][0] += CVEs_leak
            total_leak[name][1] += CVEs_leak
        else:
            total["-frameworks-"][1] += CVEs
            total_leak["-frameworks-"][1] += CVEs_leak
            for app in frameworks[name]:
                total[app][1] += CVEs
                total_leak[app][1] += CVEs_leak

    print()
    print("App\tCVEs\tCVEs w/ leak")
    for app in total.keys():
        print(app,
              "\t", "%d/%d" % (total[app][0], total[app][1]),
              "\t", "%d/%d" % (total_leak[app][0], total_leak[app][1]),
        )


def main(*args):
    parser = argparse.ArgumentParser()

    parser.add_argument("input_dir")

    options = parser.parse_args(args)
    args = vars(options)
    run(**args)


if __name__ == "__main__":
    sys.exit(main(*sys.argv[1:]))
