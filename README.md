# README #

This repository contains the raw information used in the security evaluation of the paper

	Vilanova et al., "DATS - Refactoring Access Control Out of Web Applications", ASPLOS'18
	
You can obtain the CVE numbers for the application security table by running the following command:

	cd /path/to/this/repo
	./security.py .